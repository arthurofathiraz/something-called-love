<?php
if (!defined("ADMINPATH")) {
    exit;
}

echo '
			</div>
		</div>
		<footer>
			<div class="container-fluid">
				<p class="copyright juicycodes">
					Designed & Crafted With <i class="fa fa-heart heart"></i> by <a href="https://drive.akademikuliner.com">DRIVE.AKADEMIKULINER.COM</a>.
				</p>
			</div>
		</footer>
	</div>
</div>
';
$html->GetFooter();
